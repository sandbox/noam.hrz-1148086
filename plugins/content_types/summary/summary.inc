<?php

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Node summary example.'),
  'description' => t('Node summary example.'),
  'required context' => array(
    // The node author.
    new ctools_context_required(t('User'), 'user'),
    // The node we view.
    new ctools_context_required(t('Node'), 'node'),
  ),
  'category' => t('Node'),
);

/**
 * Render callback.
 */
function node_preview_example_summary_content_type_render($subtype, $conf, $args, $context) {
  // Seperating the context to two different variables
  list($user_context, $node_context) = $context;

  // Make sure that context variable are not empty.
  if (empty($user_context) || empty($user_context->data)) {
    return;
  }

  if (empty($node_context) || empty($node_context->data)) {
    return;
  }

  $block = new stdClass();
  $block->module = 'node_preview_example';
  $block->title = t('Content summary');

  $params = array(
    // The User that write the post.
    '@name' => format_username($user_context->data),
    // The date the post has been created.
    '@date' => format_date($node_context->data->created),
    // The type of the node.
    '@type' => $node_context->data->type,
  );
  $block->content = t('This @type was written by @name at @date.', $params);
  return $block;
}

/**
 * Form callback.
 */
function node_preview_example_summary_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}